package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.app.global.SessionManager;
import com.example.app.global.WebClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    EditText email,password;
    Button btn, btnRegister;
    View.OnClickListener listener;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getApplicationContext());
        setContentView(R.layout.activity_login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.pass);
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params = new RequestParams();
                params.add("email",email.getText().toString());
                params.add("pass", password.getText().toString());
                WebClient.post("checkLogin", params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.d("login", "onSuccess: "+responseString);
                        if (responseString.contains("FAIL")){
                            Toast.makeText(getApplicationContext(),"Bad login", Toast.LENGTH_SHORT).show();
                        }else{
                            String id = responseString.replace("OK ","");
                            sessionManager.addValue("id",id);
                            startMain();
                        }
                    }
                });
            }
        };
        btn = this.<Button>findViewById(R.id.loginBtn);
        btn.setOnClickListener(listener);
        btnRegister = findViewById(R.id.register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegister();
            }
        });
        if (sessionManager.isLoggedIn())
            startMain();
    }

    public void startMain(){
        sessionManager.setLogin(true);
        Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }
    public void startRegister(){
        sessionManager.setLogin(true);
        Intent myIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        LoginActivity.this.startActivity(myIntent);

    }
}
