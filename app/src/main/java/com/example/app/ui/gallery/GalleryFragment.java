package com.example.app.ui.gallery;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.app.R;
import com.example.app.global.SessionManager;
import com.example.app.global.WebClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class GalleryFragment extends Fragment {

    double total = 0;
    private SessionManager sessionManager;
    private TextView total_ex;
    private Spinner spn;
    private List<String> catArr;
    private ArrayAdapter<String> adapter;
    private Spinner exType;
    private EditText amount;
    private EditText comment;
    private Button save;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        sessionManager = new SessionManager(getContext());
        total_ex = root.findViewById(R.id.total_ex);
        exType = root.findViewById(R.id.type);
        amount = root.findViewById(R.id.fieldAmount2);
        comment = root.findViewById(R.id.fieldComment2);
        save = root.findViewById(R.id.saveBtn2);
        spn = root.findViewById(R.id.catType);
        catArr = new ArrayList<>();
        catArr.addAll(Arrays.asList(getResources().getStringArray(R.array.exp50_array)));
        //catArr = getResources().getStringArray(R.array.exp50_array);
        adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, catArr);
        spn.setAdapter(adapter);
        loadEx();
        exType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Spinner select", "onItemSelected: "+exType.getSelectedItem().toString().split(" ")[1]);
                catArr.clear();
                if (exType.getSelectedItem().toString().split(" ")[1].equals("30")){
                    //adapter.addAll(getResources().getStringArray(R.array.exp30_array));
                    catArr.addAll(Arrays.asList(getResources().getStringArray(R.array.exp30_array)));
                }else{
                    catArr.addAll(Arrays.asList(getResources().getStringArray(R.array.exp50_array)));
                    //adapter.addAll(getResources().getStringArray(R.array.exp50_array));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddEx();
            }
        });
        return root;
    }


    private void loadEx(){
        WebClient.get("getTotal/"+sessionManager.getValue("id"),null, new TextHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject data = new JSONObject(responseString);
                    Log.d("user", "onSuccess: "+responseString);
                    //balance.setText("Balance: "+(double)(user.getDouble("balance")/100)+"€");
                    total_ex.setText(data.getDouble("total")/100+"€");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }
        });
    }
    private void AddEx(){
        RequestParams params = new RequestParams();
        params.add("user_id",sessionManager.getValue("id"));
        params.add("amount",amount.getText().toString());
        params.add("comment",comment.getText().toString());
        params.add("exType",exType.getSelectedItem().toString().split(" ")[1]);
        params.add("type",spn.getSelectedItem().toString().toLowerCase());
        WebClient.post("expenses", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loadEx();
            }
        });
    }
}
