package com.example.app.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.app.R;
import com.example.app.global.SessionManager;
import com.example.app.global.WebClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private TextView balance;
    private SessionManager sessionManager;
    private JSONObject user;
    private MaterialProgressBar perc50;
    private MaterialProgressBar perc30;
    private MaterialProgressBar perc20;
    private EditText amount;
    private EditText comment;
    private Spinner exType;
    private Button addBtn;
    private Button addBalance;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        sessionManager = new SessionManager(getContext());
        /*
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        balance = root.findViewById(R.id.balance);
        perc50 = root.findViewById(R.id.perc50);
        perc30 = root.findViewById(R.id.perc30);
        perc20 = root.findViewById(R.id.perc20);


        loadEx();
        addBalance = root.findViewById(R.id.topUp);
        addBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddBalance();
            }
        });

        return root;
    }

    public void dialogAddBalance(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add balance");
// I'm using fragment here so I'm using getView() to provide ViewGroup
// but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog, (ViewGroup) getView(), false);
// Set up the input
        final EditText amount = (EditText) viewInflated.findViewById(R.id.dialogAmount);
        final EditText comment = (EditText) viewInflated.findViewById(R.id.dialogComment);
        comment.setVisibility(View.GONE);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

// Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                addBalance(amount.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void addBalance(String amount){

        RequestParams params = new RequestParams();
        params.add("user_id",sessionManager.getValue("id"));
        params.add("amount",amount);
        WebClient.post("balance", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loadEx();
            }
        });
    }


    private void loadEx(){
        WebClient.get("users/"+sessionManager.getValue("id"),null, new TextHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    user = new JSONObject(responseString);
                    Log.d("user", "onSuccess: "+responseString);
                    balance.setText("Balance: "+(double)(user.getDouble("balance")/100)+"€");
                    perc50.setProgress(user.getInt("perc_50"));
                    perc30.setProgress(user.getInt("perc_30"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }
        });
    }

    private void AddEx(){
        RequestParams params = new RequestParams();
        params.add("user_id",sessionManager.getValue("id"));
        params.add("amount",amount.getText().toString());
        params.add("comment",comment.getText().toString());
        params.add("exType",exType.getSelectedItem().toString().split(" ")[1]);
        WebClient.post("expenses", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loadEx();
            }
        });
    }
}
