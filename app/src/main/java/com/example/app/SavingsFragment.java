package com.example.app;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.global.SessionManager;
import com.example.app.global.WebClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class SavingsFragment extends Fragment {


    private SessionManager sessionManager;
    private MaterialProgressBar perc;
    private TextView total_save;
    private Button saveDiag, setGoal;

    public SavingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sessionManager = new SessionManager(getContext());

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_savings, container, false);

        perc = view.findViewById(R.id.save_perc);
        total_save = view.findViewById(R.id.total_save);
        saveDiag = view.findViewById(R.id.addsave);
        setGoal = view.findViewById(R.id.setGoal);
        saveDiag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSetSave();
            }
        });
        setGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSetGoal();
            }
        });


        loadSave();
        return view;
    }


    private void loadSave(){
        WebClient.get("savings/"+sessionManager.getValue("id"),null, new TextHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject save = new JSONObject(responseString);
                    Log.d("savings", "onSuccess: "+responseString);
                    total_save.setText((double)(save.getDouble("total")/100)+"€");
                    perc.setProgress(save.getInt("reach"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }
        });
    }

    public void dialogSetSave(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add savings");
// I'm using fragment here so I'm using getView() to provide ViewGroup
// but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog, (ViewGroup) getView(), false);
// Set up the input
        final EditText amount = (EditText) viewInflated.findViewById(R.id.dialogAmount);
        final EditText comment = (EditText) viewInflated.findViewById(R.id.dialogComment);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

// Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                saveSavings(amount.getText().toString(),comment.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    public void dialogSetGoal(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Set goal");
// I'm using fragment here so I'm using getView() to provide ViewGroup
// but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog, (ViewGroup) getView(), false);
// Set up the input
        final EditText amount = (EditText) viewInflated.findViewById(R.id.dialogAmount);
        final EditText comment = (EditText) viewInflated.findViewById(R.id.dialogComment);
        comment.setVisibility(View.GONE);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

// Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                setGoal(amount.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void saveSavings(String amount, String comment){

        RequestParams params = new RequestParams();
        params.add("user_id",sessionManager.getValue("id"));
        params.add("amount",amount);
        params.add("comment",comment);
        params.add("name","");
        WebClient.post("savings", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
               loadSave();
            }
        });
    }

    public void setGoal(String amount){
        RequestParams params = new RequestParams();
        params.add("user_id",sessionManager.getValue("id"));
        params.add("amount",amount);
        WebClient.post("goal", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loadSave();
            }
        });
    }
}
