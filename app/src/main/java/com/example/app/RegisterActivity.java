package com.example.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.app.global.WebClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends AppCompatActivity {


    EditText email,password;
    Button btn, btnBack;
    View.OnClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email = findViewById(R.id.email);
        password = findViewById(R.id.pass);

        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params = new RequestParams();
                params.add("email",email.getText().toString());
                params.add("pass", password.getText().toString());
                WebClient.post("register", params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.d("register", "onSuccess: "+responseString);
                        if (responseString.contains("FAIL")){
                            Toast.makeText(getApplicationContext(),"Bad information", Toast.LENGTH_SHORT).show();
                        }else{
                            String id = responseString.replace("OK ","");
                        }
                    }
                });
            }
        };
        btn = this.<Button>findViewById(R.id.register);
        btn.setOnClickListener(listener);
        btnBack = this.findViewById(R.id.loginBtn);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBack();
            }
        });
    }


    public void startBack(){
        finish();
        /*
        Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        RegisterActivity.this.startActivity(myIntent);
        finish();
        */
    }

}
